package edu.udel.validations;

/**
 * This class will used for form item types
 * @author ASari
 *
 */
public class ItemTypes {
	
	/**
	 * PHONE_USA accepted formats
	 * xxxxxxxxxx (10 digit)
	 * xxx-xxx-xxxx
	 */
	public static final int PHONE_USA = 1;
	public static final int PHONE_INT = 2;
	public static final int MONEY     = 3;
	public static final int DATE      = 4;
	public static final int TEXT_MIN  = 5; // accepts ONLY AZaz and {space}
	public static final int TEXT_MAX  = 6; // accepts all chars
}