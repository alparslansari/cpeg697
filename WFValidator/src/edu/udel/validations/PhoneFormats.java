package edu.udel.validations;

/**
 * This class will be used to define different phone formats.
 * @author ASari
 *
 */
public class PhoneFormats {
	
	/**
	 * Ten digit phone number.
	 * Example: 3028312425
	 * Example: 1234567890 (XXXXXXXXXX)
	 */
	public static final int PHONE_USA_RAW_LEN10 = 1;
	
	/**
	 * Eleven digit phone number.
	 * Example: 13028312425
	 * Example: 01234567890 (XXXXXXXXXXX)
	 */
	public static final int PHONE_USA_RAW_LEN11 = 2;
	public static final int PHONE_INT = 3;
	
	// xxx-xxx-xxxx
	public static final int PHONE_USA_FORMATTED_1 = 4;
}