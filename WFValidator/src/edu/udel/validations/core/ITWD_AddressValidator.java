package edu.udel.validations.core;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ITWD_AddressValidator {

	Pattern zipPattern    = Pattern.compile("\\d{5}");
	Pattern zipPatternExt = Pattern.compile("\\d{4}");
	Pattern zipAll        = Pattern.compile("\\d{5}(-\\d{4})?");
	Pattern statePattern  = Pattern.compile("AL|AK|AR|AZ|CA|CO|CT|DC|DE|FL|GA|HI|IA|ID|IL|IN|KS|KY|LA|MA|MD|ME|MI|MN|MO|MS|MT|NC|ND|NE|NH|NJ|NM|NV|NY|OH|OK|OR|PA|RI|SC|SD|TN|TX|UT|VA|VT|WA|WI|WV|WY");
	//Pattern cityPattern   = Pattern.compile("([a-zA-Z]+|[a-zA-Z]+\\s[a-zA-Z]+)");
	Pattern cityPattern   = Pattern.compile("^([a-zA-Z\u0080-\u024F]+(?:. |-| |'))*[a-zA-Z\u0080-\u024F]*$");
	
	public boolean checkZipCodeAll(String zipCode)
	{
		Matcher matcher = zipAll.matcher(zipCode.trim());
	    if (matcher.matches()) {
	    	return true;
	    }
	    else
	    {
	    	return false;
	    }
	}
	
	public boolean checkZipCode(String zipCode)
	{
		Matcher matcher = zipPattern.matcher(zipCode.trim());
	    if (matcher.matches()) {
	    	return true;
	    }
	    else
	    {
	    	return false;
	    }
	}
	
	public boolean checkZipCodeExt(String zipCodeExt)
	{
		Matcher matcher = zipPatternExt.matcher(zipCodeExt.trim());
	    if (matcher.matches()) {
	    	return true;
	    }
	    else
	    {
	    	return false;
	    }
	}
     
	public boolean isValidUSAState(String state)
	{
		Matcher matcher = statePattern.matcher(state.toUpperCase().trim());
		if (matcher.matches()) 
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	
	public boolean isValidCity(String city)
	{
		Matcher matcher = cityPattern.matcher(city.trim());
		if (matcher.matches()) 
		{
			return true;
		}
		else 
		{
			return false;
		}
	}	
}