package edu.udel.validations.core;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.validator.routines.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

public class ITWD_EmailValidator {
	
	String regExpn = "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@(udel\\.edu)$";
	String regExpn2 = "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@([A-Za-z0-9-]+\\.)?(udel\\.edu)$";
	
	/**
	 * validateEmail: This function validates if given email is in valid format.
	 * It returns true if given email is valid otherwise returns false. <br>
	 * This is a generic email validation function.
	 * @param email
	 * @return True: if given email is valid otherwise return false
	 */
	public boolean validateEmail(String email)
	{
		//EmailValidator ev = new EmailValidator();
		EmailValidator validator = EmailValidator.getInstance();
		if (validator.isValid(email)) {
		   // email syntax is valid, do extra validation
			InternetAddress internetAddress;
			try {
				internetAddress = new InternetAddress(email);
				//System.out.println("validateEmail:'"+email+"'");
				internetAddress.validate();
			} catch (AddressException e) {
				//System.out.println("validateEmail:'"+email+"'");
				return false;
			}
            
			return true;
		} else {
		   // email syntax is not invalid, return false
			return false;
		}
	}
	
	
	/**
	 * checkUDEmailRegex: Validate UD email using regular expression
	 * @param email
	 * @return
	 */
	private boolean checkUDEmailRegex(String email)
	{
		Pattern pattern = Pattern.compile(regExpn2, Pattern.CASE_INSENSITIVE);
	    Matcher matcher = pattern.matcher(email);
	    
	    if(!matcher.matches())
	    	System.out.println("checkUDEmailRegex '"+email+"'");

	    return matcher.matches();
	}
	
	/**
	 * checkUDEmailSimple: Validate UD email checking if ends with udel.edu
	 * @param email
	 * @return
	 */
	private boolean checkUDEmailSimple(String email)
	{
		boolean valid = false;
		if(email.indexOf("@") > 0 && email.toLowerCase().endsWith("udel.edu"))
			valid = true;
		else
		{
			System.out.println("checkUDEmailSimple '"+email+"'");
			valid = false;
		}
			
		return valid;
	}
	
	// White space is handled with .trim()
	/**
	 * checkUDEmail: This function validates given email by checkUDEmailSimple and checkUDEmailRegex.
	 * This function just validates the syntax of email.
	 * If given email is valid return true otherwise false;
	 * @param email
	 * @return True: if given email is valid otherwise return false
	 */
	public boolean checkUDEmail(String email)
	{
		// IF given email validated by checkUDEmailSimple and checkUDEmailRegex given email is valid return true;
		if(checkUDEmailSimple(email.trim()) && checkUDEmailRegex(email.trim()))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}