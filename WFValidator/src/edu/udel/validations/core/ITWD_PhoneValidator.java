package edu.udel.validations.core;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ITWD_PhoneValidator {
	private static final int maxLimit = 17;
	private static final int maxLimitInt = 25;
	Pattern pattern0   = Pattern.compile("\\d{10}");
	Pattern pattern1   = Pattern.compile("[1]\\d{10}");
	//Pattern patternInt = Pattern.compile("^\\+(?:[0-9] ?){6,14}[0-9]$");
	Pattern patternInt = Pattern.compile("^(\\+|00)(?:[0-9] ?){6,14}[0-9]$");
	
	//Pattern pattern2 = Pattern.compile("\\d{3}-\\d{7}");
	//Pattern pattern3 = Pattern.compile("\\d{3}-\\d{3}-\\{4}");
	
	public boolean validateUsaRAWPhoneLen10(String phone)
	{
		// Phone number should not be null
		if(phone==null)
		{
			System.out.println("<NULL> Phone Number must be in the form XXXXXXXXXX");
			return false;
		}
	    
		Matcher matcher = pattern0.matcher(phone);
	    if (matcher.matches()) {
	    	System.out.println("Phone Number Valid: "+phone);
	    }
	    else
	    {
	    	System.out.println("Phone Number("+phone+") must be in the form XXXXXXXXXX");
	    	return false;
	    }
		return true;
	}

	public boolean validateUsaRAWPhoneLen11(String phone)
	{
		// Phone number should not be null
		if(phone==null)
		{
			System.out.println("<NULL> Phone Number must be in the form 1XXXXXXXXXX");
			return false;
		}
		
		Matcher matcher = pattern1.matcher(phone);  
	    if (matcher.matches()) {
	    	System.out.println("Phone Number Valid: "+phone);
	    }
	    else
	    {
	    	System.out.println("Phone Number("+phone+") must be in the form 1XXXXXXXXXX");
	    	return false;
	    }
		return true;
	}
	
	public boolean validateUSAFlexible(String phone)
	{
		// Phone number should not be null
		if(phone==null)
		{
			System.out.println("<NULL> Phone Number must be in the form 1XXXXXXXXXX");
			return false;
		}
		
		if(phone.length()>maxLimit)
		{
			System.out.println("<Error> Phone Number '"+phone+"' length is greater than max limit: "+maxLimit);
			return false;
		}
		
		String newPhone = phone.replaceAll("\\D+","");
		if(newPhone.length() == 10)
		{
			validateUsaRAWPhoneLen10(newPhone);
		}
		else if(newPhone.length() == 11)
		{
			validateUsaRAWPhoneLen11(newPhone);
		}
		else
		{
			System.out.println("<Error> Phone Number '"+phone+"' contains "+newPhone.length() +" digits");
			return false;
		}
		
		return true;
	}
	
	public boolean validateInternationalPhone(String phone)
	{
		// Phone number should not be null
		if(phone==null)
		{
			System.out.println("<NULL> Phone Number must be in the form 1XXXXXXXXXX");
			return false;
		}
		else if(phone.length()>maxLimitInt)
		{
			System.out.println("<Error> Phone Number '"+phone+"' length is greater than max limit: "+maxLimit);
			return false;
		}
		else if (isAlphanumeric(phone))
		{
			System.out.println("String pattern has found in given phone number!");
			return false;
		}
			
		
		String newPhone = "+" + phone.replaceAll("\\D+","");
		Matcher matcher = patternInt.matcher(newPhone);  
	    if (matcher.matches()) {
	    	System.out.println("Phone Number Valid: "+phone);
	    	return true;
	    }
	    else
	    {
	    	System.out.println("Phone Number("+phone+") must be in the form 1XXXXXXXXXX");
	    	return false;
	    }
		
	}
	
	public boolean isAlphanumeric(String str) {
        for (int i=0; i<str.length(); i++) {
            char c = str.charAt(i);
            if (Character.isLetter(c))
                return true;
        }

        return false;
    }
}