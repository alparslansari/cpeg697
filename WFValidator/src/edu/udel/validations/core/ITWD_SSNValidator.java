package edu.udel.validations.core;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ITWD_SSNValidator {

	Pattern pattern0 = Pattern.compile("^(\\d{3}-?\\d{2}-?\\d{4})$");
	
	/**
	 * checkSSN: This function is for validating SSNs
	 * @param ssn
	 * @return
	 */
	public boolean checkSSN(String ssn)
	{
		Matcher matcher = pattern0.matcher(ssn.trim());
	    if (matcher.matches()) 
	    {
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * checkNationalID: This function is designed to validate national id's
	 * @param nationalId
	 * @return
	 */
	public boolean checkNationalID(String nationalId)
	{
		nationalId = nationalId.trim();
		if (nationalId != null && nationalId.length() == 9)
	    {
			for (int i=0 ; i < 9 ; i++)
			{
				if ( ! Character.isDigit(nationalId.charAt(i)) )
				{
					System.out.println("National ID must contain only digits");
					return false;
				}
			}
		}
		else
		{
			System.out.println("National ID must be exactly nine digits");
			return false;
		}
	    
		// everthing ok
	    return true;
	}
	
}