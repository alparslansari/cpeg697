package edu.udel.validations.core;

import edu.udel.data.UDelPerson;
import edu.udel.data.UDelPersonFactory;

public class ITWD_UDValidator {
	
	/**
	 * isValidGivenEmplid: This function takes emplid and tries to create UDEL Person
	 * if it is successful this function returns true; 
	 * Also this means given emplid is valid and in UD-Database
	 * Otherwise this function returns false. 
	 * @param emplid
	 * @return
	 */
	public boolean isValidGivenEmplid(String emplid)
    {
        UDelPerson person = null;
        try {
        	person = UDelPersonFactory.createPersonbyEmplid(emplid);
        }
        catch(Exception e) {
        	//LOG.error("isGivenEmplidValid()", e);
        	System.out.println("isValidGivenEmplid()" + e);
        }
        if(person == null)
                return false;
        return true;
    }
	
	
	/**
	 * isValidGivenEmail: This function takes a UD email and tries to create UDEL Person with email
	 * if it is successful this function returns true; 
	 * Also this means given email is valid and in UD-Database
	 * Otherwise this function returns false. 
	 * @param emplid
	 * @return
	 */
	public boolean isValidGivenEmail(String email)
    {
        UDelPerson person = null;
        try {
        	person = UDelPersonFactory.createPersonbyEmail(email);
        }
        catch(Exception e) {
                //LOG.error("isGivenEmplidValid()", e);
        	System.out.println("isGivenEmplidValid()" + e);
        }
        if(person == null)
                return false;
        return true;
    }
}