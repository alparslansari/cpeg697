package edu.udel.validations.utils;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import edu.udel.validations.datatypes.Required;

public class Commons {
	
	Required req = new Required();
	
	public boolean notBlankNumeric(String field)
    {
		if (req.requiredCheck(field))
        {
        	for (int i=field.length()-1; i>=0; i--)
        	{
                if (!Character.isDigit(field.charAt(i))) 
                {
                    return false;
                }
            }
            return true;
        }
        else
        {
            return false;
        }
    }


	public boolean validNumber(String numVal)
	{
	    if ( convNumber(numVal) != null )
	      return true;
	    else
	      return false;
	 }


	private BigDecimal convNumber(String numVal)
	{  
	    BigDecimal convNum = null;
	
	    if (numVal != null)
	    {
	       numVal = numVal.replaceAll("$", "");
	       numVal = numVal.replaceAll(",", "");
	    }
	
	    try
	    {
	      if ( numVal != null && !numVal.equals("") )
	      {
	         convNum = new BigDecimal(numVal);
	         return convNum;
	      }
	      else
	         return null;
	    }
	    catch (NumberFormatException ne)
	    {
	       //LOG.debug("INVALID NUMBER: " + ne);
	       return null;
	    }
	}
	
	
	//public SimpleDateFormat sdf2 = new SimpleDateFormat("MM/dd/yyyy");
	
	public boolean validateDateType(String dt)
	{
		if (req.requiredCheck(dt))
		{	
			try {
				SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy");
                sdf.parse(dt);
                return true;
            } catch (ParseException e) {
                return false;
            }
		}
		else
		{
			return false;
		}
	}
	
	public boolean validateLength(String field, int maxLen)
	{
		if(field == null || field.length() > maxLen)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	// This function is for checking text area type inputs find possible xss attacks
	public boolean checkXSSheuristic(String field)
	{
		boolean flag1 = field.indexOf("<") > 0;
		boolean flag2 = field.indexOf(">") > 0;
		boolean flag3 = field.toLowerCase().indexOf("&lt;") > 0;
		boolean flag4 = field.toLowerCase().indexOf("&gt;") > 0;
		
		if (flag1 | flag2 | flag3 | flag4)
			return false;
		else
			return true;
	}
}