package edu.udel.validations;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.udel.validations.core.ITWD_AddressValidator;

public class AddressTestSuite {

	ITWD_AddressValidator av = new ITWD_AddressValidator();
	
	@Test
	public void zipCodeValidTrue() {
		assertTrue(av.checkZipCode("19711"));
	}
	
	@Test
	public void zipCodeExtValidTrue() {
		assertTrue(av.checkZipCodeExt("4350"));
	}
	
	@Test
	public void zipCodeValidFalse1() {
		assertFalse(av.checkZipCode("19711a"));
	}
	
	@Test
	public void zipCodeValidFalse2() {
		assertFalse(av.checkZipCode("1971a"));
	}
	
	@Test
	public void zipCodeValidFalse3() {
		assertFalse(av.checkZipCode("attack"));
	}
	
	@Test
	public void zipCodeAllValidTrue1() {
		assertTrue(av.checkZipCodeAll("19711"));
	}
	
	@Test
	public void zipCodeAllValidTrue2() {
		assertTrue(av.checkZipCodeAll("19711-4350"));
	}
	
	@Test
	public void zipCodeAllValidFalse1() {
		assertFalse(av.checkZipCodeAll("19711a"));
	}
	
	@Test
	public void zipCodeAllValidFalse2() {
		assertFalse(av.checkZipCodeAll("1971a"));
	}
	
	@Test
	public void zipCodeAllValidFalse3() {
		assertFalse(av.checkZipCodeAll("attack"));
	}
	
	@Test
	public void zipCodeExtValidFalse1() {
		assertFalse(av.checkZipCodeExt("435a"));
	}
	
	@Test
	public void zipCodeExtValidFalse2() {
		assertFalse(av.checkZipCodeExt("aaaa"));
	}
	
	@Test
	public void stateValidTrue1() {
		assertTrue(av.isValidUSAState("TX"));
	}
	
	@Test
	public void stateValidTrue2() {
		assertTrue(av.isValidUSAState("DE"));
	}
	
	@Test
	public void stateValidTrue3() {
		assertTrue(av.isValidUSAState("PA"));
	}
	
	@Test
	public void stateValidFalse1() {
		assertFalse(av.isValidUSAState("PAA"));
	}
	
	@Test
	public void stateValidFalse2() {
		assertFalse(av.isValidUSAState("XX"));
	}
	
	@Test
	public void stateValidFalse3() {
		assertFalse(av.isValidUSAState("<script>"));
	}
	
	@Test
	public void cityValidTrue1() {
		assertTrue(av.isValidCity("Austin"));
	}
	
	@Test
	public void cityValidTrue2() {
		assertTrue(av.isValidCity("New York"));
	}
	
	@Test
	public void cityValidTrue3() {
		assertTrue(av.isValidCity("toRonTo"));
	}
	
	@Test
	public void cityValidTrue4() {
		assertTrue(av.isValidCity("St. Catharines"));
	}
	
	@Test
	public void cityValidTrue5() {
		assertTrue(av.isValidCity("Val-d'Or"));
	}
	
	@Test
	public void cityValidTrue6() {
		assertTrue(av.isValidCity("Presqu'ile"));
	}
	
	@Test
	public void cityValidTrue7() {
		assertTrue(av.isValidCity("Niagara on the Lake"));
	}
	
	@Test
	public void cityValidTrue8() {
		assertTrue(av.isValidCity("Niagara-on-the-Lake"));
	}
	
	@Test
	public void cityValidTrue9() {
		assertTrue(av.isValidCity("villes du Qu�bec"));
	}
	
	@Test
	public void cityValidTrue10() {
		assertTrue(av.isValidCity("Provence-Alpes-C�te d'Azur"));
	}
	
	@Test
	public void cityValidTrue11() {
		assertTrue(av.isValidCity("�le-de-France"));
	}
	
	@Test
	public void cityValidTrue12() {
		assertTrue(av.isValidCity("Gar�ab�r"));
	}
	
	@Test
	public void cityValidTrue13() {
		assertTrue(av.isValidCity("N�ewiller-pr�s-lauterbourg"));
	}
	
	@Test
	public void cityValidFalse1() {
		assertFalse(av.isValidCity("A----B"));
	}

	@Test
	public void cityValidFalse2() {
		assertFalse(av.isValidCity("------"));
	}
	
	@Test
	public void cityValidFalse3() {
		assertFalse(av.isValidCity("&nsbp;"));
	}
	
	@Test
	public void cityValidFalse4() {
		assertFalse(av.isValidCity("(newark)"));
	}
	
	@Test
	public void cityValidFalse5() {
		assertFalse(av.isValidCity("?Newark/"));
	}
}