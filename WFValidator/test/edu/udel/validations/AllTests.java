package edu.udel.validations;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ AddressTestSuite.class, CommonsTestSuite.class,
		EmailTestSuite.class, PhoneTestSuite.class, SSNTestSuite.class,
		UDValidatorSuite.class })
public class AllTests {

}

// This class is for running all other test cases.