package edu.udel.validations;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.udel.validations.utils.Commons;

public class CommonsTestSuite {
	
	Commons cms = new Commons();

	@Test
	public void validateLengthTrue1() {
		assertTrue(cms.validateLength("700992703", 9));
	}
	
	@Test
	public void validateLengthTrue2() {
		assertTrue(cms.validateLength("", 12));
	}
	
	@Test
	public void validateLengthFalse1() {
		String test = null;
		assertFalse(cms.validateLength(test, 12));
	}
	
	@Test
	public void validateLengthFalse2() {
		assertFalse(cms.validateLength("70099370312", 9));
	}
	
	@Test
	public void validateDateTrue1() {
		assertTrue(cms.validateDateType("12/12/12"));
	}
	
	@Test
	public void validateDateTrue2() {
		assertTrue(cms.validateDateType("2/12/12"));
	}
	
	@Test
	public void validateDateTrue3() {
		assertTrue(cms.validateDateType("2/1/12"));
	}
	
	@Test
	public void validateDateTrue4() {
		assertTrue(cms.validateDateType("12/12/2012"));
	}
	
	@Test
	public void validateDateFail1() {
		assertFalse(cms.validateDateType("//"));
	}
	
	@Test
	public void validateDateFail2() {
		assertFalse(cms.validateDateType("/12/12"));
	}
	
	@Test
	public void validateDateFail3() {
		assertFalse(cms.validateDateType("12/12/"));
	}
	
	@Test
	public void validateDateFail4() {
		assertFalse(cms.validateDateType("12//12"));
	}
	
	@Test
	public void validateDateFail5() {
		assertFalse(cms.validateDateType("a/b/c"));
	}
	
	@Test
	public void validateDateFail6() {
		assertFalse(cms.validateDateType("attack"));
	}
	
	@Test
	public void validateDateFail7() {
		assertFalse(cms.validateDateType("12/12/a"));
	}
	
	@Test
	public void validateDateFail8() {
		assertFalse(cms.validateDateType("12/12/a1"));
	}
	
	@Test
	public void isNumTrue1() {
		assertTrue(cms.notBlankNumeric("0123456789"));
	}
	
	@Test
	public void isNumFalse1() {
		assertFalse(cms.notBlankNumeric(""));
	}
	
	@Test
	public void isNumFalse2() {
		String test = null;
		assertFalse(cms.notBlankNumeric(test));
	}
	
	@Test
	public void isNumFalse3() {
		assertFalse(cms.notBlankNumeric("123123a213"));
	}
}