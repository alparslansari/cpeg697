package edu.udel.validations;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import edu.udel.validations.core.ITWD_EmailValidator;

@RunWith(JUnit4.class)
public class EmailTestSuite {

	ITWD_EmailValidator ev = new ITWD_EmailValidator();
	
	@Test
	public void validateEmailTrue0() {
		// Just a valid UD Email example
		assertTrue (ev.validateEmail("asari@udel.edu"));
	}
	
	@Test
	public void validateEmailTrue1() {
		// Just a valid UD Email example
		assertTrue (ev.validateEmail("alparslansari@udel.edu"));
	}
	
	@Test
	public void validateEmailTrue2() {
		// Just a valid UD Email example
		assertTrue (ev.validateEmail("alparslansari@gmail.com"));
	}
	
	@Test
	public void validateEmailFalse1() {
		// Just a valid UD Email example
		assertFalse (ev.validateEmail("alparslansari@gmail.cox"));
	}
	
	@Test
	public void validateEmailFalse2() {
		// Just a valid UD Email example
		assertFalse (ev.validateEmail("alpi@gmail.cxx"));
	}
	
	
	
	@Test
	public void UDemailSouldbeTrue0() {
		// Just a valid UD Email example
		assertTrue (ev.checkUDEmail("asari@udel.edu"));
	}
	
	@Test
	public void UDemailSouldbeTrue1() {
		// whitespace email whitespace 
		assertTrue (ev.checkUDEmail("  asari@udel.edu  "));
	}
	
	@Test
	public void UDemailSouldbeTrue2() {
		// whitespace email whitespace 
		assertTrue (ev.checkUDEmail("  asari@bartol.udel.edu  "));
	}
	
	@Test
	public void UDemailSouldbeTrue3() {
		// whitespace email whitespace 
		assertTrue (ev.checkUDEmail("353444@udel.edu"));
	}
	
	@Test
	public void UDemailSouldbeFalse0() {
		// Not valid email extension udelx
		assertFalse (ev.checkUDEmail("asari@null;udel.edu"));
	}
	
	@Test
	public void UDemailSouldbeFalse1() {
		// Not valid email extension udelx
		assertFalse (ev.checkUDEmail("asari@udelx.edu"));
	}
	
	@Test
	public void UDemailSouldbeFalse2() {
		// Not valid email extension edux
		assertFalse (ev.checkUDEmail("asari@udel.edux"));
	}
	
	@Test
	public void UDemailSouldbeFalse3() {
		// Not valid email extension edux
		assertFalse (ev.checkUDEmail("asari@.udel.edu"));
	}
	
	@Test
	public void UDemailSouldbeFalse4() {
		// Not valid email extension edux
		assertFalse (ev.checkUDEmail("asari@as.as.udel.edu"));
	}
	
	@Test
	public void UDemailSouldbeFalse5() {
		// Not valid email extension edux
		assertFalse (ev.checkUDEmail("asari@as@as.udel.edu"));
	}
}