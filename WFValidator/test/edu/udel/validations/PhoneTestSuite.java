package edu.udel.validations;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.udel.validations.core.ITWD_PhoneValidator;

public class PhoneTestSuite {
	
	ITWD_PhoneValidator pv = new ITWD_PhoneValidator();
	
	@Test
	public void usaRAWPhoneLen10True() {
		// True for 10 digit numbers
		assertTrue(pv.validateUsaRAWPhoneLen10("1234567890"));
	}
	
	@Test
	public void usaRAWPhoneLen11True() {
		// True for 11 digit numbers
		assertTrue(pv.validateUsaRAWPhoneLen11("12345678901"));
	}
	
	@Test
	public void USAFlexibleTrue1() {
		// True for 11 digit numbers
		assertTrue(pv.validateUSAFlexible("3027225554"));
	}
	
	@Test
	public void USAFlexibleTrue2() {
		// True for 11 digit numbers
		assertTrue(pv.validateUSAFlexible("13027225554"));
	}
	
	@Test
	public void USAFlexibleTrue3() {
		// True for 11 digit numbers
		assertTrue(pv.validateUSAFlexible("+13027225554"));
	}
	
	@Test
	public void USAFlexibleTrue4() {
		// True for 11 digit numbers
		assertTrue(pv.validateUSAFlexible("+1-302-722-5554"));
	}
	
	@Test
	public void USAFlexibleTrue5() {
		// True for 11 digit numbers
		assertTrue(pv.validateUSAFlexible("+1(302)722-5554"));
	}
	
	@Test
	public void USAFlexibleTrue6() {
		// True for 11 digit numbers
		assertTrue(pv.validateUSAFlexible("+1 (302) 722-5554"));
	}
	
	@Test
	public void USAFlexibleTrue7() {
		// True for 11 digit numbers
		assertTrue(pv.validateUSAFlexible("+1 302 722 5554"));
	}
	
	@Test
	public void USAFlexibleTrue8() {
		// True for 11 digit numbers
		assertTrue(pv.validateUSAFlexible("+1 302 722 55 54"));
	}
	
	@Test
	public void USAFlexibleFalse1() {
		// 
		assertFalse(pv.validateUSAFlexible("123456789a"));
	}
	
	@Test
	public void USAFlexibleFalse2() {
		// 
		assertFalse(pv.validateUSAFlexible("+1 (302) 722 55541"));
	}
	
	@Test
	public void USAFlexibleFalse3() {
		// 
		assertFalse(pv.validateUSAFlexible("+1 (302) 722"));
	}
	
	@Test
	public void USAFlexibleFalse4() {
		// Attack patterns should not be allowed. 
		assertFalse(pv.validateUSAFlexible("<script>alert(1);</script>"));
	}
	
	@Test
	public void USAFlexibleFalse5() {
		// 
		assertFalse(pv.validateUSAFlexible(""));
	}
	
	@Test
	public void USAFlexibleFalse6() {
		// 
		String phone = null;
		assertFalse(pv.validateUSAFlexible(phone));
	}
	
	@Test
	public void USAFlexibleFalse7() {
		// 
		assertFalse(pv.validateUSAFlexible("1"));
	}
	
	@Test
	public void usaRAWPhoneLen10False1() {
		// Not all digits "123456789a"
		assertFalse(pv.validateUsaRAWPhoneLen10("123456789a"));
	}
	
	@Test
	public void usaRAWPhoneLen10False2() {
		// Phone should not be empty
		assertFalse(pv.validateUsaRAWPhoneLen10(""));
	}

	@Test
	public void usaRAWPhoneLen10False3() {
		// Phone should not be null
		String phone = null;
		assertFalse(pv.validateUsaRAWPhoneLen10(phone));
	}
	
	@Test
	public void usaRAWPhoneLen10False4() {
		// Phone should have 10 digits
		assertFalse(pv.validateUsaRAWPhoneLen10("1"));
	}
	
	@Test
	public void usaRAWPhoneLen10False5() {
		// Phone should have 10 digits: exceeding limit
		assertFalse(pv.validateUsaRAWPhoneLen10("01234567890"));
	}
	
	@Test
	public void usaRAWPhoneLen11False1() {
		// Not all digits "123456789a"
		assertFalse(pv.validateUsaRAWPhoneLen11("0123456789a"));
	}
	
	@Test
	public void usaRAWPhoneLen11False2() {
		// Phone should not be empty
		assertFalse(pv.validateUsaRAWPhoneLen11(""));
	}

	@Test
	public void usaRAWPhoneLen11False3() {
		// Phone should not be null
		String phone = null;
		assertFalse(pv.validateUsaRAWPhoneLen11(phone));
	}
	
	@Test
	public void usaRAWPhoneLen11False4() {
		// Phone should have 11 digits
		assertFalse(pv.validateUsaRAWPhoneLen11("1"));
	}
	
	@Test
	public void usaRAWPhoneLen11False5() {
		// Phone should have 11 digits: exceeding limit
		assertFalse(pv.validateUsaRAWPhoneLen11("012345678901"));
	}
	
	@Test
	public void validateInternationalPhoneTrue1()
	{
		assertTrue(pv.validateInternationalPhone("+905321837272"));
	}
	
	@Test
	public void validateInternationalPhoneTrue2()
	{
		assertTrue(pv.validateInternationalPhone("00905321837272"));
	}
	
	@Test
	public void validateInternationalPhoneTrue3()
	{
		assertTrue(pv.validateInternationalPhone("+90-532-183-7272"));
	}
	
	@Test
	public void validateInternationalPhoneTrue4()
	{
		assertTrue(pv.validateInternationalPhone("00130257211"));
	}
	
	@Test // Character O presents here
	public void validateInternationalPhoneFalse()
	{
		assertFalse(pv.validateInternationalPhone("0013025O211"));
	}
}