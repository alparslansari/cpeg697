package edu.udel.validations;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import edu.udel.validations.core.ITWD_SSNValidator;

@RunWith(JUnit4.class)
public class SSNTestSuite {

	ITWD_SSNValidator sv = new ITWD_SSNValidator();
	
	@Test
	public void SSNisValid1() {
		assertTrue(sv.checkSSN("123-45-6789"));
	}
	
	@Test
	public void SSNisValid2() {
		assertTrue(sv.checkSSN("987654321"));
	}
	
	@Test
	public void SSNisValid3() {
		assertTrue(sv.checkSSN(" 987-65-4321 "));
	}
	
	@Test
	public void SSNisValid4() {
		assertTrue(sv.checkSSN("987-65-4321 "));
	}
	
	@Test
	public void SSNisNOTValid1() {
		assertFalse(sv.checkSSN("987-65-4321 (attack)"));
	}
	
	@Test
	public void SSNisNOTValid2() {
		assertFalse(sv.checkSSN("9876-5-4321"));
	}
	
	@Test
	public void NIDisValid1() {
		assertTrue(sv.checkNationalID("123456789"));
	}
	
	@Test
	public void NIDisValid2() {
		assertTrue(sv.checkNationalID(" 987654321 "));
	}
	
	@Test
	public void NIDisNOTValid1() {
		assertFalse(sv.checkNationalID("987654321 (attack)"));
	}
	
	@Test
	public void NIDisNOTValid2() {
		assertFalse(sv.checkNationalID("9876-5-4321"));
	}
	
	@Test
	public void NIDisNOTValid3() {
		assertFalse(sv.checkNationalID("98765432a"));
	}
}