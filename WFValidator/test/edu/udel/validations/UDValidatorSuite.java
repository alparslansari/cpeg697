package edu.udel.validations;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.udel.validations.core.ITWD_UDValidator;

public class UDValidatorSuite {

	ITWD_UDValidator udv = new ITWD_UDValidator();
	
	@Test
	public void UdelIDValid() {
		assertTrue(udv.isValidGivenEmplid("700993703"));
	}

	@Test
	public void UdelIDNotValid() {
		assertFalse(udv.isValidGivenEmplid("700993703x"));
	}
	
	@Test
	public void UdelEmailValid() {
		assertTrue(udv.isValidGivenEmail("asari@udel.edu"));
	}
	
	@Test
	public void UdelEmailNotValid() {
		assertFalse(udv.isValidGivenEmail("asari@udel.edux"));
	}
}